from chatterbot import ChatBot
import chatterbot
print(chatterbot.__file__)

chatbot = ChatBot(
    'Botty',
    trainer='chatterbot.trainers.ChatterBotCorpusTrainer'
)

# Train based on the english corpus
chatbot.train(r"./training")

from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse, parse_qs


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        try:
            parsed = urlparse(self.path)
            message = parse_qs(parsed.query)['message'][0]
            response = chatbot.get_response(message);
            self.send_response(200)
            self.end_headers()
            self.wfile.write(str(response).encode("utf-8"))
        except Exception as err:
            print(err)
            self.send_response(500)
            self.end_headers()
            self.wfile.write(str(err).encode("utf-8"))


httpd = HTTPServer(('0.0.0.0', 8085), SimpleHTTPRequestHandler)
print('Helllooooo')
httpd.serve_forever()
