package main;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.PrintWriter;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.vdurmont.emoji.EmojiParser;

public class Main {

	public static void main(String[] args) throws Exception {
		File input = new File("C:\\Users\\ollevitt\\source\\repos\\pythonBot\\Hangouts.json");
		File output = new File("C:\\Users\\ollevitt\\source\\repos\\pythonBot\\training\\hangouts.yml");
		output.delete();
		int pair = 1;
		PrintWriter writer = new PrintWriter(output,"UTF-8");
		writer.println("categories:");
		writer.println("- conversations");
		writer.println("conversations:");
		writer.println("- - Hello");
		JsonFactory jfactory = new JsonFactory();
		JsonParser jParser = jfactory.createParser(input);
		
		while (jParser.nextToken() != null) {
		    String fieldname = jParser.getCurrentName();
		    if ("text".equals(fieldname)) {
		        @SuppressWarnings("unused")
				JsonToken token = jParser.nextToken();
		        
		        System.out.println(jParser.getText());
		        String text = EmojiParser.removeAllEmojis(jParser.getText().replaceAll("\"", "")).trim();
		        if (text.length() == 0)
		        	continue;
		        if (pair++ % 2 == 0) 
		        	writer.println("- - "+text);
		        else 
		        	writer.println("  - "+text);
		    }
		}
		jParser.close();
		writer.close();
	}
}
